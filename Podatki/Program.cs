﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxes
{
    class Program
    {
        static void Main(string[] args)
        {
            TakeInputCostsAndAddThem();
            TakeInputIncomesAndAddThem();
            Calculations.SubstractCostsFromIncomes();
            Calculations.CalculateTax();
            Console.WriteLine("Tax to pay: {0}", Calculations.Tax);
            Console.WriteLine("Sum of costs: {0}", Calculations.Cost);
            Console.WriteLine("Incomes: {0}", Calculations.Income);
            Console.WriteLine("Incomes minus costs: {0}", Calculations.TaxBase);
        }

        public static void TakeInputCostsAndAddThem()
        {
            string answer;
            do
            {
                Console.WriteLine("provide cost:");
                double cost = Convert.ToDouble(Console.ReadLine());
                Calculations.AddCosts(ref cost);

                Console.WriteLine("Another one? Y/N");
                answer = Console.ReadLine();
            }
            while (answer == "Y");
        }

        public static void TakeInputIncomesAndAddThem()
        {
            string answer;
            do
            {
                Console.WriteLine("provide income:");
                double income = Convert.ToSingle(Console.ReadLine());
                Calculations.AddIncomes(ref income);

                Console.WriteLine("Another one? Y/N");
                answer = Console.ReadLine();
            }
            while (answer == "Y");
        }
    }
}
