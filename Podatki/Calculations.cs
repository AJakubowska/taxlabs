﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Taxes 
{
    class Calculations
    {
        //private static double costsZUS = 199.34;
        private static double healthFeeZUS = 198.97;
        private static double cost = 199.34;
        public static double Cost
        {
            get { return cost;  }
            set { cost = value; }
        }

        private static double income = 0;
        public static double Income
        {
            get { return income; }
            set { income = value; }
        }

        public static void AddCosts(ref double parameter)
        {
            Cost += parameter;
        }

        public static void AddIncomes(ref double parameter)
        {
            Income += parameter;
        }

        public static double TaxBase { get; set; }
        public static void SubstractCostsFromIncomes()
        {
            TaxBase = Income - Cost;
        }

        public static double Tax { get; set; }
        public static void CalculateTax()
        {
            double tax = 0.18 * TaxBase;
            Tax = tax - healthFeeZUS;
        }
    }
}
